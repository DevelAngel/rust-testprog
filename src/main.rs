extern crate testlib_a;
extern crate testlib_b;

fn main() {
    println!("Hello, world!");
    let num = 5;
    println!("testlib-a({}) = {}", num, testlib_a::test_a(num));
    println!("testlib-b({}) = {}", num, testlib_b::test_b(num));
}
